# Aide et ressources de NavARP pour Synchrotron SOLEIL

[<img src="https://gitlab.com/fbisti/navarp/-/raw/develop/navarp/gui/icons/navarp.svg" width="100"/>](https://fbisti.gitlab.io/navarp/index.html)

## Résumé

- visualisation, analyse, normalisation, filtre, assemblage des données
- Open source

## Sources

- Code source:  https://gitlab.com/fbisti/navarp
- Documentation officielle:  https://fbisti.gitlab.io/navarp/

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](https://fbisti.gitlab.io/navarp/installation.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
| [Tutoriaux officiels](https://fbisti.gitlab.io/navarp/usage.html) |   | 

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS,  python
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: text
- en sortie: png,  images
- sur un disque dur
